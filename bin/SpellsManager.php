<?php
require 'DaoUtils.php';

class SpellsManager {

	// *************   Spells Data   *************
	public static function ping(){
		$response = SpellsDao::test_connection();
		
		if ($response == SpellsDao::$SUCCESSFUL_CONNECTION_RESPONSE) {
        	http_response_code(200);
        	return json_encode(['DEBUG' => $response]);
		}
        
        http_response_code(500);
        return json_encode(['ERROR' => $response]);
	}

	public static function getAllSpells($redeable){

    	if ($redeable) {
			$response = SpellsDao::getAllSpellsFromView();
    	}else{
			$response = SpellsDao::getAllSpells();
    	}
    	
    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	// *************   Finds   *************
	public static function findSpells($query_params_arr, $redeable){
		
		$request = SpellRequest::getInstanceOfQueryParams($query_params_arr, $redeable);

    	if ($redeable) {
			$response = SpellsDao::findSpellsByRequestFromView($request);
    	}else{
			$response = SpellsDao::findSpellsByRequest($request);
    	}
    	
    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

		http_response_code(200);
		return $response;
	}

	public static function findSpellById($id, $redeable){
    	if ($redeable) {
			$response = SpellsDao::findSpellByIdFromView($id);
    	}else{
			$response = SpellsDao::findSpellById($id);
    	}
    	
    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellsByName($name, $redeable){
		$name = strtolower($name);

    	if ($redeable) {
			$response = SpellsDao::findSpellsByNameFromView($name);
    	}else{
			$response = SpellsDao::findSpellsByName($name);
    	}
    	
    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	




	// *************   Misc Data   *************
	public static function getAllSpellClasses(){

		$response = SpellClassesDao::getAllClasses();

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function getAllSpellSchools(){

		$response = SpellSchoolsDao::getAllSchools();

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function getAllSpellFamilies($redeable){

    	if ($redeable) {
			$response = SpellFamiliesDao::getAllFamiliesFromView();
    	}else{
			$response = SpellFamiliesDao::getAllFamilies();
    	}

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function getAllSpellFamiliesGroupByClass(){

    	
		$families_response = SpellFamiliesDao::getAllFamilies();
		$classes_response = SpellClassesDao::getAllClasses();
    	

    	if(count($classes_response)==0 && count($families_response)){
    		http_response_code(204);
        	return null;
    	}

    	$result=self::group_families_by_class($families_response, $classes_response);

    	http_response_code(200);
		return $result;
	}

	public static function getAllSpellTargets(){

		$response = SpellTargetsDao::getAllTargets();

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function getAllSpellTypes(){

		$response = SpellTypesDao::getAllTypes();

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	// *************   Finds   *************

	public static function findSpellClassById($id){

		$response = SpellClassesDao::findClassById($id);

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellSchoolById($id){

		$response = SpellSchoolsDao::getAllSchools($id);

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellFamilyById($id, $redeable){

    	if ($redeable) {
			$response = SpellFamiliesDao::findFamilyByIdFromView($id);
    	}else{
			$response = SpellFamiliesDao::findFamilyById($id);
    	}

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellFamilyByClass($className){

		$response = SpellFamiliesDao::findFamilyByClassFromView($className);

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellTargetById($id){
		$response = SpellTargetsDao::findTargetById($id);

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	public static function findSpellTypeById($id){
		$response = SpellTypesDao::findTypeById($id);

    	if(count($response)==0){
    		http_response_code(204);
        	return null;
    	}

    	http_response_code(200);
		return $response;
	}

	static function group_families_by_class($families, $classes){
		$result;

		foreach ($classes as $spellClass) {
    
    		$family_data;
	    	foreach ($families as $family) {

			    $class_id = $family["ID_CLASE"];
		        if ($spellClass["ID"] != $class_id) {
		            continue;
		        }

			    $family_data[] = [
			    	"ID" => $family["ID"], 
			    	"NOMBRE" => $family["NOMBRE"],
			    	"COLOR" => $family["COLOR"]
			    ];
			}

	        $result[] = [
	            "ID" => $spellClass["ID"],
	            "NOMBRE" => $spellClass["NOMBRE"],
	            "COLOR" => $spellClass["COLOR"],
	            "FAMILIAS" => $family_data
	        ];
	    }

	    return $result;
	}
}