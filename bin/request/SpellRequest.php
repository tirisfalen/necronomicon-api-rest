<?php
class SpellRequest{

    private $LIMIT_DAYS_FOR_NEW_SPELLS=14;

    // Object properties
    private $id;
    private $class_identifier;  //Can be numeric ID or name
    private $family_identifier; //Can be numeric ID or name
    private $name;
    private $cost;
    private $requirement;
    private $lvl;
    private $target_identifier; //Can be numeric ID or name
    private $type_identifier;   //Can be numeric ID or name
    private $num_type;
    private $distance;
    private $effect;
    private $modification_date;
    private $legendary;
    private $school_identifier; //Can be numeric ID or name        
    private $duration;

    private $redeable; //True for numeric IDs or false for names


    public function __construct($id, 
    $class_identifier, $family_identifier, $name, 
    $cost, $requirement, $lvl, 
    $target_identifier, $type_identifier, $num_type, 
    $distance, $effect, $modification_date, 
    $legendary, $school_identifier, $duration, $redeable){

        $this->id = $id;
        $this->class_identifier = $class_identifier;
        $this->family_identifier = $family_identifier;
        $this->name = $name;
        $this->cost = $cost;
        $this->requirement = $requirement;
        $this->lvl = $lvl;
        $this->target_identifier = $target_identifier;
        $this->type_identifier = $type_identifier;
        $this->num_type = $num_type;
        $this->distance = $distance;
        $this->effect = $effect;
        $this->modification_date = $modification_date;
        $this->legendary = $legendary;
        $this->school_identifier = $school_identifier;
        $this->duration = $duration;
        $this->redeable = $redeable;

        $this->isNew=self::isNew($modification_date);
    }
    
    public static function getInstanceOfQueryParams($arr, $redeable){
        
        if($arr==null){
            return new SpellRequest(null, null, null, null, 
                                    null, null, null, null,
                                    null, null, null, null,
                                    null, null, null, null, null);
        }  

        $id = self::validateArrayValue($arr, 'id');
        $name = self::validateArrayValue($arr, 'nombre');
        $cost = self::validateArrayValue($arr, 'costo');
        $requirement = self::validateArrayValue($arr, 'requisito');
        $lvl = self::validateArrayValue($arr, 'nivel');
        $num_type = self::validateArrayValue($arr, 'num');
        $distance = self::validateArrayValue($arr, 'distancia');
        $modification_date = self::validateArrayValue($arr, 'fecha_modificacion');
        $effect = self::validateArrayValue($arr, 'efecto');
        $legendary = self::validateArrayValue($arr, 'legendario');
        $duration = self::validateArrayValue($arr, 'duracion');

        if ($redeable) {
            $class_identifier = self::validateArrayValue($arr, 'clase');
            $family_identifier = self::validateArrayValue($arr, 'familia');
            $target_identifier = self::validateArrayValue($arr, 'objetivo');
            $type_identifier = self::validateArrayValue($arr, 'tipo');
            $school_identifier = self::validateArrayValue($arr, 'escuela');
        } else {
            $class_identifier = self::validateArrayValue($arr, 'id_clase');
            $family_identifier = self::validateArrayValue($arr, 'id_familia');
            $target_identifier = self::validateArrayValue($arr, 'id_objetivo');
            $type_identifier = self::validateArrayValue($arr, 'id_tipo');
            $school_identifier = self::validateArrayValue($arr, 'id_escuela');
        }

        return new SpellRequest($id, $class_identifier, $family_identifier, 
                                $name, $cost, $requirement, $lvl, 
                                $target_identifier, $type_identifier, 
                                $num_type, $distance, $effect, 
                                $modification_date, $legendary, 
                                $school_identifier, $duration, $redeable);
    }
    
    static function validateArrayValue($arr, $field) {
        if ($arr == null) {
            return null;
        }

        if (!isset($arr[$field])) {
            return null;
        }

        if ($arr[$field] == "") {
            return null;
        }

        return $arr[$field];
    }

    function isNew($date){
        if($date==null) return true;

        $DAYS_DATE_FORMAT='%a';
        $today=date_create();
        $date=date_create($date);
        
        $days_diff = date_diff($today,$date)->format($DAYS_DATE_FORMAT);

        return ($days_diff<=$this->LIMIT_DAYS_FOR_NEW_SPELLS);
    }

    function buildFilter(){

        $filter = null;

        if ($this->name != null) {
            $filter['NOMBRE'] = strtolower($this->name);
        }

        if ($this->class_identifier != null) {

            if ($this->redeable) {
                $filter['CLASE'] = strtolower($this->class_identifier);
            }else{
                $filter['ID_CLASE'] = $this->class_identifier;
            }
        }

        if ($this->family_identifier != null) {

            if ($this->redeable) {
                $filter['FAMILIA'] = strtolower($this->family_identifier);
            }else{
                $filter['ID_FAMILIA'] = $this->family_identifier;
            }
        }

        if ($this->school_identifier != null) {

            if ($this->redeable) {
                $filter['ESCUELA'] = strtolower($this->school_identifier);
            }else{
                $filter['ID_ESCUELA'] = $this->school_identifier;
            }
        }

        return $filter;
    }

    function getQuery(){
        
        $filter = self::buildFilter();

        if ($filter == null) {
            return '';
        }

        $where = 'WHERE';
        $first_condition=false;

        foreach ($filter as $field => $value) {
            
            if ($value == null) {continue;}
                
            if ($field == "NOMBRE"){
                $field_value = "{$field} LIKE LOWER('%{$value}%')";
            }
            else{
                $value = is_numeric($value) ? $value : "LOWER('{$value}')";
                $field_value = "{$field} = {$value}";
            }
            
            if($first_condition == true){
                $where = "{$where} AND {$field_value}";
            }
            else{
                $where = "{$where} {$field_value}";
                $first_condition = true;
            }
        }

        return $where;
    }


    public function getFilter(){
        return self::buildFilter();
    }

    public function __toString(){
        return self::getQuery();
    }
}