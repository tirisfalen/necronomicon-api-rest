<?php
require 'request/SpellRequest.php';

class SpellsDao{
	public static $SUCCESSFUL_CONNECTION_RESPONSE = 'pong';

	public static function test_connection(){
		require 'conecta.php';
		
		if ($mysqli->connect_error) {
		   return "Not connected, error: " . $mysqli_connection->connect_error;
		}
	   	return self::$SUCCESSFUL_CONNECTION_RESPONSE;
	}

	public static function getAllSpells(){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_HECHIZOS} ORDER BY NOMBRE ASC";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function getAllSpellsFromView(){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_HECHIZOS} ORDER BY NOMBRE ASC";
		error_log("getAllSpellsFromView() -> {$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function getSpells($offset, $limit){
		include 'conecta.php';
		$sql = "SELECT * FROM {$T_HECHIZOS} ORDER BY NOMBRE ASC LIMIT {$offset},{$limit}";
		error_log("getSpells() -> {$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function getSpellsFromView($offset, $limit){
		include 'conecta.php';
		$sql = "SELECT * FROM {$V_HECHIZOS} ORDER BY NOMBRE ASC LIMIT {$offset},{$limit}";
		error_log("getAllSpellsFromView() -> {$sql};");
		$resultado=$mysqli->getSpellsFromView($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	// *************   FIND   *************

	public static function findSpellByIdFromView($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_HECHIZOS} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findSpellsByRequestFromView($request){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_HECHIZOS} {$request}";
		error_log("Request -> {".json_encode($request->getFilter())."}");
		error_log("findSpellsByRequestFromView() -> {$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findSpellsByRequest($request){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_HECHIZOS} {$request}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findSpellsByNameFromView($name){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_HECHIZOS} WHERE NOMBRE LIKE LOWER('%{$name}%')";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findSpellsByName($name){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_HECHIZOS} WHERE NOMBRE LIKE LOWER('%{$name}%')";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}
}
class SpellSchoolsDao{
	public static function getAllSchools(){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_ESCUELAS} ORDER BY NOMBRE";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}

	public static function findSchoolById($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_ESCUELAS} WHERE ID ={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
}
class SpellFamiliesDao{
	public static function getAllFamilies(){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_FAMILIAS} ORDER BY NOMBRE";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
	
	public static function getAllFamiliesFromView(){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_FAMILIAS} ORDER BY NOMBRE";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
	
	public static function findFamilyByIdFromView($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$V_FAMILIAS} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
	
	public static function findFamilyById($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_FAMILIAS} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
	
	public static function findFamilyByClassFromView($className){		
		include 'conecta.php';
		$sql = "SELECT ID, NOMBRE, COLOR FROM {$V_FAMILIAS} WHERE CLASE = '{$className}'";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
    	return $datos;
	}
}
class SpellClassesDao{

	public static function getAllClasses(){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_CLASES} ORDER BY NOMBRE ASC";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findClassById($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_CLASES} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}
}
class SpellTargetsDao{

	public static function getAllTargets(){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_TIPOS_OBJETIVOS} ORDER BY NOMBRE ASC";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findTargetById($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_TIPOS_OBJETIVOS} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}
}
class SpellTypesDao{
	public static function getAllTypes(){
		$page = $page * $limit;
		include 'conecta.php';
		$sql="SELECT * FROM {$T_TIPOS_HECHIZOS} ORDER BY NOMBRE";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}

	public static function findTypeById($id){
		include 'conecta.php';
		$sql="SELECT * FROM {$T_TIPOS_HECHIZOS} WHERE ID={$id}";
		error_log("{$sql};");
		$resultado=$mysqli->query($sql);
		$datos=$resultado->fetch_all(MYSQLI_ASSOC);
		return $datos;
	}
}