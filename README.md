# Necronomicon API-REST

Esta API proporciona una forma de consultar información sobre los hechizos del manual Viejo Azeroth y sus atributos, clases, escuelas, familias, objetivos y tipos asociados. La API está diseñada para ser utilizada a través de solicitudes HTTP GET y devuelve respuestas en formato JSON.

## URL de la API

La URL base de la API es `http://tu-servidor/api`. Asegúrate de que el servidor esté configurado correctamente y que el archivo `SpellsManager.php` esté disponible.

## Rutas Disponibles

A continuación, se describen las rutas disponibles y cómo usarlas:

- **Ping (Prueba de conexión)**
  - Ruta: `/ping`
  - Método: GET
  - Descripción: Utiliza esta ruta para comprobar si la API está en funcionamiento. No requiere parámetros adicionales.
  - Ejemplo de uso: `http://tu-servidor/api/ping`

- **Hechizos**
  - Ruta: `/hechizos`
  - Método: GET
  - Descripción: Obtiene una lista de hechizos mágicos. Puedes usar parámetros para filtrar y paginar los resultados.
  - Parámetros opcionales:
    - `page`: Número de página para la paginación.
    - `limit`: Límite de resultados por página.
    - `raw`: Si se establece en `true`, los resultados se mostrarán sin formato legible para humanos.
    - `nombre`: Filtra por nombre de hechizo.
    - `clase`: Filtra por la clase del hechizo.
    - `familia`: Filtra por familia de hechizo.
    - `escuela`: Filtra por escuela de hechizo.
    - `nuevo`: Filtra por hechizos nuevos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos?page=1&limit=10&nombre=hechizo1`

- **Clases de Hechizos**
  - Ruta: `/hechizos/clases`
  - Método: GET
  - Descripción: Obtiene una lista de clases de hechizos mágicos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos/clases`

- **Escuelas de Hechizos**
  - Ruta: `/hechizos/escuelas`
  - Método: GET
  - Descripción: Obtiene una lista de escuelas de hechizos mágicos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos/escuelas`

- **Familias de Hechizos**
  - Ruta: `/hechizos/familias`
  - Método: GET
  - Descripción: Obtiene una lista de familias de hechizos mágicos. Puedes filtrar por nombre de familia.
  - Parámetros opcionales:
    - `raw`: Si se establece en `true`, los resultados se mostrarán sin formato legible para humanos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos/familias?familia=familia1`

- **Objetivos de Hechizos**
  - Ruta: `/hechizos/objetivos`
  - Método: GET
  - Descripción: Obtiene una lista de objetivos de hechizos mágicos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos/objetivos`

- **Tipos de Hechizos**
  - Ruta: `/hechizos/tipos`
  - Método: GET
  - Descripción: Obtiene una lista de tipos de hechizos mágicos.
  - Ejemplo de uso: `http://tu-servidor/api/hechizos/tipos`

## Respuestas

La API devuelve respuestas en formato JSON. La estructura de las respuestas puede variar según la ruta y los parámetros utilizados en la solicitud. Las respuestas contienen información detallada sobre los hechizos y sus atributos.

Esperamos que esta documentación te ayude a utilizar la API de consulta de hechizos de manera efectiva. Si tienes alguna pregunta o necesitas más detalles, no dudes en ponerte en contacto con el administrador de la API.