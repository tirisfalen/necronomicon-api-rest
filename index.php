<?php
// sudo apt-get install php-mbstringng
require 'bin/SpellsManager.php';


$API_URL='/v1';
$TEST_CONNECTION_URL="{$API_URL}/ping";
$SPELLS_ENDPOINT="{$API_URL}/hechizos";
$SPELLS_CLASSES_ENDPOINT="{$SPELLS_ENDPOINT}/clases";
$SPELLS_SCHOOLS_ENDPOINT="{$SPELLS_ENDPOINT}/escuelas";
$SPELLS_FAMILIES_ENDPOINT="{$SPELLS_ENDPOINT}/familias";
$SPELLS_TARGETS_ENDPOINT="{$SPELLS_ENDPOINT}/objetivos";
$SPELLS_TYPES_ENDPOINT="{$SPELLS_ENDPOINT}/tipos";

$FIRST_PAGE = -1;
$NO_LIMIT = 0;

// GET

function handleSearchRequest($path) {
    global $SPELLS_ENDPOINT;
    global $SPELLS_CLASSES_ENDPOINT;
    global $SPELLS_SCHOOLS_ENDPOINT;
    global $SPELLS_FAMILIES_ENDPOINT;
    global $SPELLS_TARGETS_ENDPOINT;
    global $SPELLS_TYPES_ENDPOINT;
    global $API_URL;

    if (validatePath($path,$SPELLS_CLASSES_ENDPOINT)) {
        return handleClasses($path);
    }

    if (validatePath($path,$SPELLS_SCHOOLS_ENDPOINT)) {
        return handleSchools($path);
    }

    if (validatePath($path,$SPELLS_FAMILIES_ENDPOINT)) {
        return handleFamilies($path);
    }

    if (validatePath($path,$SPELLS_TARGETS_ENDPOINT)) {
        return handleTargets($path);
    }

    if (validatePath($path,$SPELLS_TYPES_ENDPOINT)) {
        return handleSpellsTypes($path);
    }

    if (validatePath($path,$SPELLS_ENDPOINT)) {
        return handleSpells($path);
    }

    http_response_code(404);
    return ['Error' => $path.' not found'];
}

function handleSpells($path) {
    global $SPELLS_ENDPOINT;

    $redeable = isHumanRedeable();
    $value = getValueFromPath($path, $SPELLS_ENDPOINT);
    $filter = getFilterParameters();

    if (empty($filter) && $value===null) {
        return SpellsManager::getAllSpells($redeable);
    }
    if ($value===null) {
        return SpellsManager::findSpells($filter,$redeable);
    }

    if (is_numeric($value)) {
        // TODO: Pendiente implementar busqueda por ID + filtro
        return SpellsManager::findSpellById($value, $redeable);
    }

    if (!isset($filter['nombre'])) {
        $filter['nombre'] = $value;
    }

    return SpellsManager::findSpells($filter,$redeable);   
}

function handleClasses($path){
    global $SPELLS_CLASSES_ENDPOINT;

    $id = getValueFromPath($path, $SPELLS_CLASSES_ENDPOINT);

    if ($id!==null && is_numeric($id)) {
        return SpellsManager::findSpellClassById($id);
    }

    return SpellsManager::getAllSpellClasses();
}

function handleSchools($path){
    global $SPELLS_SCHOOLS_ENDPOINT;

    $id = getValueFromPath($path, $SPELLS_SCHOOLS_ENDPOINT);

    if ($id!==null && is_numeric($id)) {
        return SpellsManager::findSpellSchoolById($id);
    }

    return SpellsManager::getAllSpellSchools();
}

function handleFamilies($path){
    global $SPELLS_FAMILIES_ENDPOINT;

    $value = getValueFromPath($path, $SPELLS_FAMILIES_ENDPOINT);
    $redeable = isHumanRedeable();
    $group = isset($_GET['group']);

    if ($value===null) {
        if ($group) {
            return SpellsManager::getAllSpellFamiliesGroupByClass();
        }
        return SpellsManager::getAllSpellFamilies($redeable);
    }

    if (is_numeric($value)) {
        return SpellsManager::findSpellFamilyById($value, $redeable);
    }

    $value = ucfirst(strtolower($value));

    return SpellsManager::findSpellFamilyByClass($value);
}

function handleTargets($path){
    global $SPELLS_TARGETS_ENDPOINT;

    $id = getValueFromPath($path, $SPELLS_TARGETS_ENDPOINT);

    if ($id!==null && is_numeric($id)) {
        return SpellsManager::findSpellTargetById($id);
    }
    
    return SpellsManager::getAllSpellTargets();
}

function handleSpellsTypes($path){
    global $SPELLS_TYPES_ENDPOINT;

    $id = getValueFromPath($path, $SPELLS_TYPES_ENDPOINT);

    if ($id!==null && is_numeric($id)) {
        return SpellsManager::findSpellTypeById($id);
    }
    
    return SpellsManager::getAllSpellTypes();
}

function getValueFromPath($path, $api_path){   
    $pathWithoutApi = str_replace($api_path, '', $path);
    
    $value = trim($pathWithoutApi, '/');

    if ($value == '') {
        return null;
    }
    
    return $value;
}

function validatePath($path, $api_path){
    error_log("Checking for {$api_path}");
    
    $path = rtrim($path, '/');

    if ($path === $api_path) {
        return true;
    }
    
    $escapedAPIPath = preg_quote($api_path, '/');
    $pattern = "/^{$escapedAPIPath}\/([\p{L}\d_]+)$/u";
    // matches a string that starts with the escaped $api_path followed by a forward slash and allows alphanumeric characters, including accented.
    // The u modifier enables UTF-8 support for accented characters.
    $fullPattern = "#^{$escapedAPIPath}{$pattern}#";

    return (bool) preg_match($pattern, $path);
}

// HTTP - GET - PARAMS

function isPaginated(){
    global $FIRST_PAGE;
    global $NO_LIMIT;

    $page = getPage();
    $limit = getLimit();
    
    if ($page == $FIRST_PAGE && $limit == $NO_LIMIT) {
        return false;
    }

    return true;
}

function getPage(){
    global $FIRST_PAGE;

    if (isset($_GET['page'])) {
        return $_GET['page']-1;
    }

    return $FIRST_PAGE;
}

function getLimit(){
    global $NO_LIMIT;

    if (isset($_GET['limit'])) {
        return $_GET['limit'];
    }
    
    return $NO_LIMIT;
}

function isHumanRedeable(){
    if (isset($_GET['raw'])) {
        return false;
    }
    return true;
}

function getFilterParameters(){
    $filter = [];

    if (isset($_GET['clase'])) {
        $filter['clase'] = $_GET['clase'];
    }

    if (isset($_GET['familia'])) {
        $filter['familia'] = $_GET['familia'];
    }

    if (isset($_GET['nuevo'])) {
        $filter['nuevo'] = $_GET['nuevo'];
    }

    if (isset($_GET['escuela'])) {
        $filter['escuela'] = $_GET['escuela'];
    }

    if (isset($_GET['nombre'])) {
        $filter['nombre'] = $_GET['nombre'];
    }

    return $filter;
}




// *********  MAIN **********

function handleRoute($path) {
    global $API_URL;
    global $TEST_CONNECTION_URL;
    
    if (validatePath($path,$TEST_CONNECTION_URL)) {
        return SpellsManager::ping();
    }

    if (strpos($path, $API_URL) !== 0) {
        http_response_code(404);
        return json_encode(['Error' => "Path {$path} doesn't start with {$API_URL}" ]);
    }

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        return json_encode(handleSearchRequest($path));
    } 
}

$request_uri = $_SERVER['REQUEST_URI'];
error_log("Working path: {$request_uri}");

$path = parse_url($request_uri)['path'];
$decodedPath = urldecode($path);
error_log("Decoded path: {$decodedPath}");

$sanitizedPath = filter_var($decodedPath, FILTER_SANITIZE_STRING);
error_log("Sanitized Path: {$sanitizedPath}");

$response = handleRoute($sanitizedPath);

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");
print_r($response);